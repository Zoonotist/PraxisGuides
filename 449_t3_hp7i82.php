<!DOCTYPE html>
<!--[if IE 7 ]><html lang="en" class="ie ie7"><![endif]-->
<!--[if IE 8 ]><html lang="en" class="ie ie8"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>

<meta charset="utf-8">
<title>Food Not Bombs</title>
<meta name="Robots" content="INDEX,FOLLOW">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--[if (gt IE 6)&(lt IE 9)]>
	<link rel="stylesheet" media="screen" href="_global/styles/ie-legacy.css">
	<script src="_global/scripts/html5-shim.js"></script>
	<![endif]-->
<!--[if gt IE 8]><!-->
<link rel="stylesheet" media="screen" href="_global/styles/base.css">
<link rel="stylesheet" media="screen" href="_global/styles/responsive.css">
<!--<![endif]-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel='stylesheet' type='text/css'>
<style>
      body {
        font-family: 'Open Sans Condensed', sans-serif;
	font-weight: 600; 
        font-size: 16px;
      }
    </style>
</head>
<body>
<div id="fnblogo"><img src="images/FNB_logo_color.png" height="110px" width="110px"></div>
<div id="l-container" class="nav-slide-panel">
<header id="l-masthead" class="fixedNav">
<div class="l-wrap">
<h1 id="site-logo"><a class="link-home animated zoomInRight" href="index.php">FOOD NOT BOMBS</a></h1>
<div id="nav-global">
<h2 class="head"><a class="trigger" href="#">Site Navigation</a></h2>
<ul class="global-nav">
<center>
<li class="global-nav-item">
<a class="homeLink global-nav-link" href="index.php">Home</a>
</li>
<li class="global-nav-item">
<a class="eventsLink global-nav-link" href="events.php">Events</a>
</li>
<li class="global-nav-item">
<a class="newsLink global-nav-link" href="news.php">News</a>
</li>
<li class="global-nav-item">
<a class="faqLink global-nav-link" href="faq.php">FAQ</a>
</li>
<li class="global-nav-item">
<a class="volunteerLink global-nav-link" href="volunteer.php">Volunteer</a>
</li>
<li class="global-nav-item">
<a class="donateLink global-nav-link" href="donate.php">Donate</a>
</li>
<li class="global-nav-item">
<a class="locationsLink global-nav-link" href="contacts.php">Locations</a>
</li>
</center>

</ul>
</div>
<a class="to-bottom" href="#l-footer">Jump to Bottom of Page</a>
</div>
<div class="spacer"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>

	
	//pure javascript
	var pathname = window.location.pathname;
	console.log(pathname);
	if (pathname == "/new_site/index.php"){
		$(".homeLink").addClass("activeLink");
		 console.log('you are on the home page');
	}
	if (pathname == "/new_site/events.php"){
		$('.link-home').text('FNB: EVENTS');
		$(".eventsLink").addClass("activeLink");
		 console.log('you are on the events page');
	}
	if (pathname == "/new_site/news.php"){
		$('.link-home').text('FNB: NEWS');
		$(".newsLink").addClass("activeLink");
		 console.log('you are on the news page');
	}
	if (pathname == "/new_site/faq.php"){
		$('.link-home').text('FNB: FAQS');
		$(".faqLink").addClass("activeLink");
		 console.log('you are on the faq page');
	}
	if (pathname == "/new_site/volunteer.php"){
		$('.link-home').text('FNB: VOLUNTEER');
		$(".volunteerLink").addClass("activeLink");
		 console.log('you are on the volunteer page');
	}
	if (pathname == "/new_site/donate.php"){
		$('.link-home').text('FNB: DONATE');
		$(".donateLink").addClass("activeLink");
		 console.log('you are on the donate page');
	}
	if (pathname == "/new_site/contacts.php"){
		$('.link-home').text('FNB: LOCATIONS');
		$(".locationsLink").addClass("activeLink");
		 console.log('you are on the locations page');
	}
	if (pathname == "/new_site/speaker.php"){
		$('.link-home').text('FNB: 2016 TOUR');
		
		}
		 if (pathname == "/new_site/story.php"){
		$('.link-home').text('FNB: OUR STORY');
		
	}
	 if (pathname == "/new_site/seven-steps.php"){
		$('.link-home').text('FNB: SEVEN STEPS');
		
	}
	if (pathname == "/new_site/hungry_for_peace.php"){
		$('.link-home').text('FNB: HUNGRY FOR PEACE');
		
	}
   

</script>
</header><div style="padding: 50px;">
<p> <center>
<h2 class="boxed-title"><strong class="boxed-title-box box-basics">SEVEN STEPS TO STARTING A FOOD NOT BOMBS CHAPTER</strong></h2>
</center>
<br><br><big>At the outset, starting a Food Not Bombs might seem like more than
you can handle. Work on the basics, taking one step at a time. There is
no need to feel pressured into accomplishing everything all at once. It
might take a couple of weeks to get things rolling, or it may take
months. One person cannot be a Food Not Bombs group, but one person can
initiate a group.</big></p>
<p> <big>Once you have made the decision to
start a local Food Not Bombs group, pick a meeting date, time, and place
and gather together everyone interested to talk about what you would
like to do. You might start with a group of friends, or members of an
existing group, or it could be people who respond to posters and emails
announcing your intentions.</big></p>
<p><big> The following is a step-by-step process to get your food
operation up and running. Because of your unique situation, you may need
to add, ignore or reorder steps. Follow the path you feel will work best
for your group.</big></p>
<br><br><br>
<h2 class="boxed-title" style="font-size:30px;"><strong class="boxed-title-box box-basics">STEP 1</strong></h2>
<p> <BIG>Start by getting a phone number,
email or postal address. By using a voice mailbox you can have an
outgoing message with information about the next meeting time and place,
and receive messages so that you never miss a call. You can also post a
MySpace, Facebook, Twitter or other website for your group. Many
chapters also create listserves on sites like Riseup, Google or Yahoo.
Some groups use a commercial mailbox or post office box for their
physical address. After initially setting up your chapter's contact information, please share the contact details with us using
<a HREF="https://foodnotbombs.net/info/mapForm">this form</a> so your details can be added to our site.
</BIG> </p><br><br><br>
<h2 class="boxed-title" style="font-size:30px;"><strong class="boxed-title-box box-basics">STEP 2</strong></h2>
<p> <BIG> Make flyers announcing the
existence of your new local Food Not Bombs chapter with your contact
information and time, date and location of your planning meetings. By
handing out these flyers at events, posting them around town, emailing
the announcement of your next meeting to friends, classmates and other
local organizations, you will get additional volunteers and donations of
food and supplies. It is helpful to have regularly scheduled weekly or
monthly meetings and always know the date of the next one to share at
the end of each meeting. Encourage people who may rely on the food to
participate in the meetings. Post the time, date and location of your
meetings on your websites. We provide sample flyers that you can down
load on www.foodnotbombs.net. The agenda for your first meetings can
include a.) food collection b.) location for cooking c.) locations to
deliver your food. d.) consider a location to share your meals in the
future e.) outreach to people who may want to volunteer and join the
meal.</BIG></p>
<br><br><br>
<h2 class="boxed-title" style="font-size:30px;"><strong class="boxed-title-box box-basics">STEP 3</strong></h2>
<p> <BIG> Arrange for the use of a vehicle. Among the members of
your group, there might be enough vehicles of the right size for your
needs, but if not, you might be able to borrow a van or truck from a
sympathetic church group or similar organization. If none of the above
succeeds, you can always hold fund-raising events specifically for the
purchase of a van or truck. Some groups use bicycles and bike carts to
pickup the food and take the meals and literature out to share. Other
groups use shopping carts and travel on public transportation.</BIG></p>
<br><br><br>
<h2 class="boxed-title" style="font-size:30px;"><strong class="boxed-title-box box-basics">STEP 4</strong></h2>
<p>
<BIG>With flyers in hand, begin looking for sources of food. The
first places to approach are the local food co-ops, produce warehouses,
farmers markets, organic food stores and bakeries. These types of stores
tend to be supportive and are a good place to practice your approach.
Tell them you plan to share the food with the hungry, delivering food to
shelters and soup kitchens as well as providing a regular meal once the
chapter is established, and if they are interested and willing, arrange
for a regular time to pick up the food each week or as often as is
practical. Where it is appropriate, leave literature which explains the
mission of Food Not Bombs. </BIG></p>
<br><br><br>
<h2 class="boxed-title" style="font-size:30px;"><strong class="boxed-title-box box-basics">STEP 5</strong></h2>
<p> <BIG>Start by delivering
your collected food to housing projects, shelters and local meal
programs. It is important to get to know the food pantries and soup
kitchens in your area. Learn where they are located, whom they serve,
and how many they serve. This information will help you plan your
delivery route and distribute the appropriate types and amounts of food
to each program. This will also give you an idea of when and where your
chapter should start to share your regularly prepared meals. It is
usually desirable to arrange a regular delivery schedule with each
location. Building relationships with the other food programs is
valuable.</BIG> </p>
<br><br><br>
<h2 class="boxed-title" style="font-size:30px;"><strong class="boxed-title-box box-basics">STEP 6</strong></h2>
<p> <BIG>Once this network becomes
established, start to skim some food out of the flow without disrupting
the program. With this food, prepare meals to serve on the streets with
literature about Food Not Bombs as well as current issues and related
events. It can be very helpful to share meals at rallies and
demonstrations first; there your group can recruit more volunteers,
collect donations, as well as lift the spirits of those participating in
the action. Giving out meals at a protest can build community and
supports the cause in a very direct way. </BIG></p>
<br><br><br>
<h2 class="boxed-title" style="font-size:30px;"><strong class="boxed-title-box box-basics">STEP 7</strong></h2>
<p> <BIG>Once enough people are involved, consider sharing meals in
a visible way one day a week to the hungry on the streets. Cooking and
serving food there builds community within the group and is hard work,
but this is also great fun. Choose a time, day and location where you
will reach the most people. Always be on time. Pick highly visible
locations where a diverse population is likely to walk past your food
and literature.</BIG></p>
<p>
One aspect of our mission is to help make the &#34;invisible
poverty&#34; more visible. Your meal is not a Food Not Bombs meal if you don&#39;t provide literature and display a banner. Otherwise the public will think you are a church and have the impression your group believes that our political and economic system is fine and that all we need to do is care for those who are not able to make it.
</p>
<p>
We are not a charity, we are seeking to build a movemnet to end the exploitation of the economic and political system.
If you are not interested in changing society so no one needs to eat at a soup kitchen then you might want to volunteer with a church or government food program. We are seeking to end hunger and poverty not just feed it.
When so much money is spent on the military we know it is possible to create a world where no one is required to stand in line to eat at a charity.
The meal and literature can be a powerful way to reach out to the community with the message of &#34;<b>food not
bombs.&#34;
</b></p>
<br><br><br>
<h2 class="boxed-title"><strong class="boxed-title-box box-basics">RESOURCES TO HELP YOU START YOUR FOOD NOT BOMBS GROUP</strong></h2>
<a HREF="sample_agenda.php">SAMPLE AGENDA FOR
THE FIRST MEETINGS</a> <br> When you hold your first few meetings you can use this sample agenda.
You can move the times to fit your schedule and add items you think might be important.
You may be starting your group because of a crisis and that item could be placed on your agenda.
</p>
<p> <a HREF="principles.php">THE PRINCIPLES OF FOOD
NOT BOMBS</a> <br> Volunteers participating in the 1992 and 1995 gatherings
came to consensus that we would have three principles that would make us FOOD NOT BOMBS. 1.The food is vegan and free to all. 2. We have no leaders and use the process of consensus to make desisions. 3. That Food Not Bombs is dedicated to nonviolent direct action towards creating a world free from domination, coercion and violence.
</p>
<p> <a HREF="https://foodnotbombs.net/info/mapForm">LIST YOUR GROUP ON WWW.FOODNOTBOMBS.NET</a> <br>
Please fill out this <a HREF="https://foodnotbombs.net/info/mapForm">form</a> to list the details of your local Food Not Bombs group.
</p>
<p> <a HREF="food_safety.php">FOOD SAFETY FOR FOOD
NOT BOMBS</a> <br> Our food is always vegan or vegetarian and is one reason our food is safe and that no one has been made ill eating with Food Not Bombs. If meat or dairy is included in the food you recover you can donate it to a food program that does provide meat and dairy. Many people that want to contribute to Food Not Bombs are not informed about our principle of only sharing plant based foods.
</p>
<p>
<a HREF="literature_table.php">SOLIDARITY NOT CHARITY - REMEMBER - LITERATURE AND A
BANNER AT EVERY MEAL - </a> <br>We encourage all Food Not Bombs groups to have a stack of these flyers on their literature table at every meal. This is very important to do if your chapter is in the United States where the governmnet has taken measures to reduce our meals to just another charity.
</p>
<p> <a HREF="PDF/money_spent_flyer_color.pdf" target="_blank">DOWNLOAD THIS FLYER </a><br>
<p>This flyer is an effective way to let everyone in your community to know about your Food Not Bombs group. You can post two or three copies on top of one another and cut the tags so people can take your contact information. You may have a volunteer that is not able to help your group the day of your meal but thay could post these flyers all over town once a month. (Again cut the tags for best results)
</p>
<a HREF="PDF/enjoy_a_meal.pdf" target="_blank">DOWN LOAD THIS FLYER</a><br>AND ADD YOUR DETAILS TO
LET PEOPLE KNOW WHEN AND WHERE YOU ARE SHARING YOUR REGULAR MEAL
</p>
<p>
<a HREF="PDF/seven_steps.pdf" target="_blank">DOWN LOAD THIS FLYER </a><br>ON SEVEN STEPS TO STARTING A FOOD NOT BOMBS GROUP
</p>
<p>
<a HREF="PDF/good_samaritan_act.pdf" target="_blank">THE GOOD SAMARITAN ACT</a> - You can give this flyer published by Second Harvest to grocery and bakery staff to show they are protected from liability.
</p>
<p>
<a HREF="http://foodnotbombs.net/flyers.html" target="_blank">FLYERS TO SHARE AT YOUR MEALS AND EVENTS</a>
</p>
<p>
<a HREF="hungry_for_peace.php"><b>THE FOOD NOT BOMBS
HANDBOOK</b></a>
</p>
<p> <center>
YOU CAN GET STARTED TODAY! Call 1-800-884-1136 of
email us at <a HREF="/cdn-cgi/l/email-protection#a4c9c1cad1e4c2cbcbc0cacbd0c6cbc9c6d78acac1d0"><span class="__cf_email__" data-cfemail="afc2cac1daefc9c0c0cbc1c0dbcdc0c2cddc81c1cadb">[email&#160;protected]</span></a><br>and let us
know you are starting a Food Not Bombs chapter in your community. We would be happy to add your chapter&#39;s contact information and schedule on our website.
</center><br></p>
</div>
<div class="row cols-2">
<div class="col col1">
<hgroup>
<h2 class="upper">Make a Donation</h2>
<h3>We accept credit,checks,paypal, and love!</h3><br>
<a class="btn btn-action" href="donate.php">Donate Today<span class="ico ico-arr-tan">&nbsp;</span></a>
</hgroup>
<ul>
</ul>
</div>
<div class=" col col2">
<hgroup>
<h2 class="upper">Volunteer</h2>
<h3>We can always use a little help!</h3><br>
<a class="btn btn-action" href="volunteer.php">Volunteer Now<span class="ico ico-arr-tan">&nbsp;</span></a>
</hgroup>
<ul>
</ul>
</div>
</div>
<div class="row cols-2 border">
<div class="col col1">
<hgroup>
<h2 class="upper">Search GoodSearch</h2>
<h3>A search engine that makes a difference.</h3><br>
<a class="btn btn-action" href="http://www.goodsearch.com/">Visit GoodSearch<span class="ico ico-arr-tan">&nbsp;</span></a>
</hgroup>
</div>
<div class=" col col2">
<hgroup>
<h2 class="upper">Automatic Monthly Donations</h2>
<h3>Become a monthly donator, and become part of the contributors list!</h3><br>
<a class="btn btn-action" href="donatemonthly.php">Automatic Monthly Help<span class="ico ico-arr-tan">&nbsp;</span></a>
</hgroup>
</div>
</div>
<p>If you would like to donate more than $1 please feel free to add any amount you want to the Paypal form.<br><br>
If you are interested in making a larger donation of more then $100 or more and request a tax deduction for your contribution, please contact us at 575-770-3377. </p>
</div>
<div class="m-top"><a href="#">Back&nbsp;to&nbsp;top<span class="icon-up">&nbsp;&uarr;</span></a></div>
</div>
<div id="l-mega">
<div class="l-wrap m-collapse collapse-footer row cols-4">
<div class="col">
<h2 class="collapse-head"><a href="http://foodnotbombs.net" style="cursor: none;">FNB-Network</a></h2>
<ul class="collapse-list">
<li class="collapse-item"><a class="collapse-link" href="story.php">Our Story</a></li>
<li class="collapse-item"><a class="collapse-link" href="events.php">Events</a></li>
<li class="collapse-item"><a class="collapse-link" href="faq.php">FAQ's</a></li>
<li class="collapse-item"><a class="collapse-link" href="http://blog.foodnotbombs.net">Blog</a></li>
<li class="collapse-item"><a class="collapse-link" href="http://foodnotbombs.net/bookad.html">Store</a></li>
<li class="collapse-item"><a class="collapse-link" href="contacts.php">Locations</a></li>
<li class="collapse-item"><a class="collapse-link" href="webcollective.php">Global Coordination</a></li>

</ul>
</div>
<div class="col">
<h2 class="collapse-head"><a href="http://fnb-freeskool.org" target="_blank" style="cursor: none;">Free-Skool</a></h2>
<ul class="collapse-list">
<li class="collapse-item"><a class="collapse-link " href="http://fnb-freeskool.org/mission_statement.html">Mission Statement</a></li>
<li class="collapse-item"><a class="collapse-link " href="http://fnb-freeskool.org/free_skool_photos.html">Photos</a></li>
<li class="collapse-item"><a class="collapse-link" href="http://fnb-freeskool.org/application.html">Applications</a></li>
</ul>
</div>
<div class="col">
<h2 class="collapse-head"><a href="#" style="cursor: none;">Lend a Helping Hand</a></h2>
<ul class="collapse-list">
<li class="collapse-item"><a class="collapse-link " href="donate.php">Donate</a></li>
<li class="collapse-item"><a class="collapse-link " href="volunteer.php">Volunteer</a></li>
<li class="collapse-item"><a class="collapse-link " href="http://foodnotbombs.net/flyers.html">Flyers</a></li>
</ul>
</div>
<div class="col">
<h2 class="collapse-head"><a href="#" style="cursor: none;">Connect</a></h2>
<ul class="collapse-list">
<li class="collapse-item"><a class="collapse-link ext-fb" href="https://www.facebook.com/FoodNotBombsGlobal" target="_blank">Facebook</a></li>
<li class="collapse-item"><a class="collapse-link ext-tw" href="https://twitter.com/Food_Not_Bombs_" target="_blank">Twitter</a></li>
<li class="collapse-item"><a class="collapse-link ext-gp" href="https://plus.google.com/" target="_blank">Google+</a></li>
<li class="collapse-item"><a class="collapse-link ext-yt" href="https://www.youtube.com/results?search_query=food+not+bombs" target="_blank">YouTube</a></li>
</ul>
</div>
</center>
</div>
</div>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>!window.jQuery && document.write('<script src="_global/scripts/jquery-1.9.1.min.js"><\/script>')</script>
<script src="_global/scripts/library.js"></script>
</body>
</html>

