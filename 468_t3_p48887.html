<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="generator" content="pandoc">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
<title>Student Organiser's Handbook - Introduction</title>
<style type="text/css">code{white-space: pre;}</style>
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
<link rel="stylesheet" href="custom.css">
</head>
<body>
<div class="divtop">
<ul class="ultop">
<li class="litop"><a class="atop" href="intro.html">Introduction</a></li>
<li class="litop"><a class="atop" href="c1.html">Chapter 1</a></li>
<li class="litop"><a class="atop" href="c2.html">Chapter 2</a></li>
<li class="litop"><a class="atop" href="c3.html">Chapter 3</a></li>
<li class="litop"><a class="atop" href="c4.html">Chapter 4</a></li>
<li class="litop"><a class="atop" href="contacts.html">Contacts</a></li>
<li class="litop"><a class="atop" href="resources.html">Resources</a></li>
<li class="litop"><a class="atop" href="glossary.html">Glossary</a></li>
</ul>
</div>
<header>
<h1 class="title">Introduction</h1>
</header>
<nav id="TOC">
<ul class="ultoc">
<li><a href="#why-organise"><span class="toc-section-number">1</span> Why Organise?</a></li>
<li><a href="#what-does-being-a-student-organiser-involve"><span class="toc-section-number">2</span> What does Being a Student Organiser Involve?</a></li>
</ul>
</nav>
<h1 id="why-organise"><span class="header-section-number">1</span> Why Organise?</h1>
<figure class="figright"><img src="images/middlesexoccupation.jpg" alt="banner that says: the university is a factory - strike, occupy!"></figure>
<p>If the university is a factory, then what does that make us? If deans and vice chancellors are the managers, and lecturers are the employees - then students are the raw material. Graduates are one of the products: trained up, compliant workers ready to be sold to the highest bidder. At least, this is what university seems to have become. Every education institution is scrambling to get “connections with industry”. Even the most theoretical subjects are advertised and valued because of the “transferable skills” they teach. The irony is that while our future employers are the people who will profit from the hard work we put into learning, it is we who have to foot the bill. The most recent graduates are going to leave university with more than 50,000 debt to the government, and even more in bank loans and overdrafts. <a href="#fn1" class="footnoteRef" id="fnref1"><sup>1</sup></a></p>
<p>At the same time, the university is less under our own control than ever before. “Managerialism” means that the people who run universities do not see themselves as benevolent leaders, running the university democratically on behalf of staff and students, but as managers running the university like any other business - in order to make a profit. <a href="#fn2" class="footnoteRef" id="fnref2"><sup>2</sup></a> Relationships with corporations go beyond just inviting them to careers fairs every year - they now tend to have a say in course content, partner in research projects, and have the ear of management <a href="#fn3" class="footnoteRef" id="fnref3"><sup>3</sup></a>. These “partners” are not just ordinary corporations but include arms dealers and oil companies. The people selling crowd control equipment to repress protest overseas are the same ones cosying up to our new managers. The companies using dirty tactics to undermine climate research in our science departments have set up camp the next building over and are recruiting graduates. Even banks are getting in on this. Santander is leading the way, and has now convinced about 70 universities in the UK alone to become “Santander Universities” with the promise of funding and internships. <a href="#fn4" class="footnoteRef" id="fnref4"><sup>4</sup></a></p>
<p>We are not the only people losing out through these changes. Lecturing at a university has gone from being a relatively nice job to one that involves years working for nothing, and high amounts of stress and pressure even if you do make it. <a href="#fn5" class="footnoteRef" id="fnref5"><sup>5</sup></a> Illnesses due to stress in university workers is on the rise. <a href="#fn6" class="footnoteRef" id="fnref6"><sup>6</sup></a> Workers who provide services such as IT, cleaning, and security are facing cuts which lead to being over-stretched and under-paid. Privatisation (outsourcing of services like IT or cafeterias to private companies) means even worse working conditions for many of the lowest-paid staff. As one anonymous worker at the University of Bristol wrote:</p>
<p><em>“Let’s be frank: students will be paying more for less. Students may have glossy magazines and fancy rooms with the latest technology, but behind the scenes there is a lot of rot, and the stench is becoming more evident. The latest cuts have resulted in the closure of courses, increased staff workloads, redundancies and job downgrading. This means overworked and demoralized teachers, larger sizes of classes, and fewer contact hours for students.”</em> <a href="#fn7" class="footnoteRef" id="fnref7"><sup>7</sup></a></p>
<p>There is a lot to be fought for - a lot we have to gain if we can change things. The question then is, what is to be done? To be honest, the prospects look bleak. Left-leaning political parties response to the current crisis has been the same as ever - to promise us something slightly less bad if we help them to power (despite the fact that every rise in tuition fees has been founded on broken election promises). The student unions and the NUS have generally done very little as well. They will organise tame action and useless negotiations when their bureaucracy is pushed from below, but otherwise tend to do nothing actually useful. It is up to us, then. If the university is going to get better, if we are going to stop the government loading us with more debt, then we must organise ourselves - from the grassroots - in order to take militant, direct action on a mass scale.</p>
<p>This is not impossible - only difficult. This decade, a mass movement of students in Germany successfully reversed the introduction of tuition fees in their country. Likewise in Quebec in 2012, people organised a ‘Student Strike’, with thousand upon thousands of students walking out of class for months on end. Along with other action, this managed to stop a hike in tuition fees. These are just a couple of examples, but the point is - it is possible to mobilise students to take militant action. ‘Student apathy’ is not an immovable barrier, just a temporary sleep that people can be roused from.</p>
<p>The idea of this guide is to provide some pointers to the student organisers of the future - so they can learn from the experience and mistakes of the people who’ve written it. But we are not experts - no one really ‘knows’ what they are doing. The difference between someone who gets active and organises, and someone who sits back because they “aren’t the right person for the job” is just this - confidence. The only way to learn to be a good organiser is to get out there and start doing it.</p>
<p class="centerpara"><em><strong>We are the ones we’ve been waiting for!</strong></em></p>
<h1 id="what-does-being-a-student-organiser-involve"><span class="header-section-number">2</span> What does Being a Student Organiser Involve?</h1>
<p>The term ‘student organiser’ is used to get away from the idea that most students are inactive and need a few superhero ‘activists’ to save the day for them.<a href="#fn8" class="footnoteRef" id="fnref8"><sup>8</sup></a> It is also used to get away from the idea that there are ‘student leaders’ who need to tell everyone else what to do. Everyone should be concerned by how managers, corporations, and the government are ruining education (not to mention our lives), and everyone can and should be resisting them.</p>
<p>To us, organising at university means:</p>
<ul>
<li>Helping students with radical politics to find each other</li>
<li>Taking direct action to improve student life in and outside of the university</li>
<li>Educating each other and persuading more people to fight back</li>
<li>Creating spaces for ‘direct democracy’ where students can get together to plan action and take back control of their university</li>
<li>Acting in ‘solidarity’ with the struggles of university workers and people outside of the university</li>
<li>Building a ‘culture of resistance’ on campus</li>
</ul>
<p>The long-term goal is to get the majority of students to organise themselves based on the principles of “direct democracy”, using mass direct action to make social change. Of course, this won’t happen all at once and we can’t make it happen just by ourselves. We have to start smaller - and this may mean taking action as a minority along the way. We learn by doing after all, and most people won’t get involved in direct action until they see examples of it working right in front of them. As such this guide is divided into five parts:</p>
<ol>
<li><strong>Small Group Organising</strong> - covers the basics of starting a group from scratch, holding meetings, keeping it going</li>
<li><strong>Events and Demonstrations</strong> - how to organise and publicise events and demonstrations</li>
<li><strong>Organising Your Department or Faculty</strong> - a guide from some students in Quebec about how they organised in their universities during the student strikes of 2012</li>
<li><strong>Occupations</strong> - a 101 on occupying university buildings</li>
<li><strong>Appendices</strong> - contains useful resources, contacts, and links to further information</li>
</ol>
<p>Some subjects are not covered here because there is a lot of information on them already, or because the authors don’t know enough. So we highly recommend you also read up on things like small group direct actions (eg blockading), first aid, theory/history, etc. The focus is on organising at universities in the UK. School students might also want to read “How to Organize a Student Revolution” by Jeremy Hammond <a href="#fn9" class="footnoteRef" id="fnref9"><sup>9</sup></a></p>
<p>Finally, this guide will only continue to be useful if it stays up to date and grows - so please consider adding contributions from your own experiences! There’s no copyright, so you’re welcome to reproduce, reprint, and rewrite it whatever way you like. See the website <a href="http://studenthandbook.ourproject.org">http://studenthandbook.ourproject.org</a> for the latest updates and contact information.</p>
<section class="footnotes">
<hr />
<ol>
<li id="fn1"><a href="https://www.bbc.co.uk/news/education-14488312">http://www.bbc.co.uk/news/education-14488312</a><a href="#fnref1">↩</a></li>
<li id="fn2">As an example of the effects this is having, see: <a href="https://bristolalternativevoice.wordpress.com/2011/11/27/back-to-the-future-humanist-values-in-higher-education/">https://bristolalternativevoice.wordpress.com/2011/11/27/back-to-the-future-humanist-values-in-higher-education/</a>; also <a href="https://bristolalternativevoice.wordpress.com/2011/05/23/routinisation-of-work-2/">https://bristolalternativevoice.wordpress.com/2011/05/23/routinisation-of-work-2/</a><a href="#fnref2">↩</a></li>
<li id="fn3">for example, the website for University of Bristol faculty of engineering currently boasts “Industry is also deeply involved in what our students are taught. All of the departments in the Faculty have an Industrial Advisory Board, formed by leading industrialists.” Note that members of the IAB at the time of writing include arms dealer Thales. See the report “Study War no More” for a wider look at the involvement of arms dealers in UK universities - <a href="http://www.studywarnomore.org.uk/">http://www.studywarnomore.org.uk/</a><a href="#fnref3">↩</a></li>
<li id="fn4"><a href="http://www.santander.co.uk/uk/santander-universities">http://www.santander.co.uk/uk/santander-universities</a><a href="#fnref4">↩</a></li>
<li id="fn5"><a href="https://bristolalternativevoice.wordpress.com/2011/12/09/a-career-in-academia-twenty-years-of-schooling-and-theyll-put-you-on-the-day-shift-2/">https://bristolalternativevoice.wordpress.com/2011/12/09/a-career-in-academia-twenty-years-of-schooling-and-theyll-put-you-on-the-day-shift-2/</a><a href="#fnref5">↩</a></li>
<li id="fn6"><a href="https://bristolalternativevoice.wordpress.com/2012/03/14/workloads-and-yet-more-workloads/">https://bristolalternativevoice.wordpress.com/2012/03/14/workloads-and-yet-more-workloads/</a><a href="#fnref6">↩</a></li>
<li id="fn7"><a href="https://bristolalternativevoice.wordpress.com/2011/11/27/open-letter-to-the-students/">https://bristolalternativevoice.wordpress.com/2011/11/27/open-letter-to-the-students/</a><a href="#fnref7">↩</a></li>
<li id="fn8">See the text “Give up Activism” published in Do or Die issue 9, for a discussion on “the activist mentality” – <a href="http://www.eco-action.org/dod/no9/activism.htm">http://www.eco-action.org/dod/no9/activism.htm</a><a href="#fnref8">↩</a></li>
<li id="fn9"><a href="http://www.school-survival.net/kit/revolution.php">http://www.school-survival.net/kit/revolution.php</a><a href="#fnref9">↩</a></li>
</ol>
</section>
</body>
</html>
